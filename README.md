# Sermon Notes

- Creates styled Evernotes that are branded.
- Generates YouVersion specific links for Bible References.
- Generates a (bit.ly) / chrch.ly link


###TODO: 
- Modify custom keyword for bitly
- Automate sharing to social networks (Facebook, Twitter, Table Project)
	- link and message
- Add support for other youversion events 
	- youversion://bookmarks 
	- youversion://reading_plans