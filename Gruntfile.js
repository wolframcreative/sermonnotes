module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> Created with MEAN stack by Wolfram Creative http://wolframcreative.com */\n'
			},
			build: {
				files: {
					'www/js/app.min.js': 'www/js/app.js',
					'www/js/vendor/dep.min.js': 'www/js/vendor/dependencies.js'
				}

			}
		},
		concat: {
			dev: {
				files: {
					'www/js/app.js': ['src/app/app.js', 'src/app/router.js',
						'src/app/**/*.js'
					],
					'www/js/vendor/dependencies.js': ['vendor/js/*.js',
						'vendor/showdown/src/showdown.js',
						'vendor/showdown/src/extensions/*.js'
					]
				}

			}
		},
		compass: {
			dev: {
				options: {
					banner: '/*! <%= pkg.name %> - <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					specify: 'src/sass/style.scss',
					sassDir: 'src/sass/',
					cssDir: 'www/css/'
				}
			}
		},
		watch: {
			scripts: {
				files: ['src/**/*.js', 'vendor/**/*.js', '*.js', 'src/**/*.scss'],
				tasks: ['concat', 'compass'],
				options: {
					spawn: false,
					livereload: true
				},
			},
		},
		nodemon: {
			dev: {
				options: {
					file: 'server.js',

					ignoredFiles: ['README.md', 'node_modules/**'],
					watchedExtensions: ['js'],

					delayTime: 1,
					legacyWatch: true,
					env: {
						PORT: '1330'
					},
					cwd: __dirname
				}
			}
		},

		concurrent: {
			target: {
				tasks: ['nodemon', 'watch'],
				options: {
					logConcurrentOutput: true
				}
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-nodemon');
	grunt.loadNpmTasks('grunt-concurrent');



	// Default task(s).
	grunt.registerTask('server', ['concurrent', 'concat:dev']);
	grunt.registerTask('default', ['compass', 'concat', 'uglify']);


};
