// karma.conf.js
module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai','sinon-chai'],
    files: [
    
    'src/app/*.js',
    'src/app/*/*.js',

	'test/app/*.spec.js',
    'test/app/*/*.spec.js',
    ],
    browsers: ['PhantomJS']
  });
};