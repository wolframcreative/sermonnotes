app.config(['$routeProvider', '$locationProvider', function($routeProvider,
	$locationProvider) {

	$locationProvider.html5Mode(true);

	$routeProvider.when('/', {
		templateUrl: '/src/app/views/home.ng',
		controller: 'HomeController'
	});

	$routeProvider.when('/login', {
		templateUrl: '/src/app/views/login.ng',
		controller: 'LoginController'
	});

	$routeProvider.when('/create/note/:notebookID', {
		templateUrl: '/src/app/views/note.ng',
		controller: 'NoteController',
	});

	$routeProvider.when('/create/template', {
		templateUrl: '/src/app/views/templateNote.ng',
		controller: 'TemplateController'
	});

	// $routeProvider.when('/settings', {
	//   templateUrl: '/src/app/views/settings.ng',
	//   controller: 'SettingsController'
	// });

	// $routeProvider.when('/template/edit/:id', {
	//   templateUrl: 'src/app/views/template/editTemplate.ng',
	//   controller: 'EditTemplateController'
	// });

	// $routeProvider.when('/template/copy/:id', {
	//   templateUrl: 'src/app/views/template/copyTemplate.ng',
	//   controller: 'CopyTemplateController'
	// });

	// $routeProvider.when('/template/delete/:id', {
	//   templateUrl: 'src/app/views/template/editTemplate.ng',
	//   controller: 'EditTemplateController'
	// });

	$routeProvider.when('/dashboard', {
		templateUrl: '/src/app/views/list.ng',
		controller: 'ListController'
	});

	$routeProvider.otherwise({
		redirectTo: '/'
	});
}]);
