app.controller("NoteController", ['$scope', '$interpolate', '$location',
  '$routeParams',
  '$http', 'apiCall',
  function($scope, $interpolate, $location, $routeParams, $http, apiCall) {
    $scope.pageClass = 'page-create';

    var converter = new Showdown.converter({
      extensions: ['youversion']
    });
    $scope.note = {
      title: "",
      date: "",
      teacher: "",
      sermon_name: "",
      bible_reference: "",
      note_content: "",
      note_template: "",
      note_converted: "",
      note_organization: ""
    };


    $scope.convert = function() {
      $scope.note.note_content = converter.makeHtml($scope.note.note_content);
    }

    $scope.preview = function() {
      var templateParse = $scope.selectedOption;

      templateParse = templateParse.replace('[[title]]', '{{note.title}}')
        .replace('[[date]]', '{{note.date}}')
        .replace('[[teacher]]', '{{note.teacher}}')
        .replace('[[sermon_name]]', '{{note.sermon_name}}')
        .replace('[[bible_reference]]',
          '{{note.bible_reference}}')
        .replace('[[note_content]]', '{{note.note_content}}');
      return templateParse;
    }

    $scope.create = function() {
      $scope.loading = true;
      $scope.note.guid = $routeParams.notebookID;
      $scope.note.note_template = $scope.selectedOption;
      // $scope.note.note_organization = 'rockypeak';

      apiCall.createNote($scope.note).success(function(body, statusCode) {
        $scope.loading = false;
        $scope.note = {
          title: "",
          date: "",
          teacher: "",
          sermon_name: "",
          bible_reference: "",
          note_content: ""
        };
        $scope.bitlyUrl = body.data.link_edit.link;
      });
    };

    if (local.get('templates').length === 0 || typeof local.get('templates') ===
      'undefined') {

      apiCall.getTemplates().then(function(templates) {
        if (Object.keys(templates.data).length > 0) {
          $scope.note_templates = templates.data;
          $scope.selectedOption = $scope.note_templates[0].content;
        } else {
          $scope.noTemplates = true;
        }


      });
    } else {
      $scope.note_templates = local.get('templates');
      $scope.selectedOption = $scope.note_templates[0].content;
    }

  }
]).directive('compile', ['$compile', function($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compile);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
    );
  };
}]);
