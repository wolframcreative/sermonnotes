app.controller("HomeController", ['$scope', '$location', '$rootScope',
  'AuthenticationService',
  function($scope, $location, $rootScope, AuthenticationService) {
    $scope.pageClass = 'page-home';
    $scope.credentials = {
      email: "",
      password: ""
    };

    $rootScope.$on('new_user', function() {
      $scope.newUser = true;
    });

    if (local.get('logged_in')) {
      $location.url('/dashboard');
    }

    $scope.login = function() {
      AuthenticationService.login($scope.credentials);
    };

    $scope.register = function() {
      AuthenticationService.register($scope.credentials);
    }

    $scope.logout = function() {
      AuthenticationService.logout();
    };

  }
]);
