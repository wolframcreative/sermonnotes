app.controller("ListController", ['$scope', 'apiCall', function($scope, apiCall) {
  $scope.pageClass = 'page-dashboard';
  var note_templates = local.get('templates'),
    notebook_list = local.get('notebooks');


  if (!notebook_list) {
    apiCall.listNotebooks().success(function(s) {
      $scope.notebooks = s;
      local.write('notebooks', s);
    }).error(function(e) {
      $scope.notebooks = e;
    });
  } else {
    $scope.notebooks = notebook_list;
  }


  if (!note_templates) {
    apiCall.getTemplates().then(function(templates) {
      if (Object.keys(templates.data).length > 0) {
        $scope.note_templates = templates.data;
        local.write('templates', templates.data);
      } else {
        $scope.noTemplates = true;
      }
    });
  } else {
    $scope.note_templates = note_templates;
  }

  $scope.deleteTemplate = function(event) {
    var _id = $(event.target).data('id');
    event.preventDefault();
    apiCall.deleteTemplate({
      id: _id
    }).then(function(template) {
      console.log(template);
    });
  }
}]);
