app.controller("TemplateController", ['$scope', '$location', '$routeParams',
  '$http', 'apiCall',
  function($scope, $location, $routeParams, $http, apiCall) {
    $scope.pageClass = 'page-template';
    $scope.templateNote = {
      name: '',
      content: '',
      organization: ''
    };
    $scope.createTemplate = function() {
      $scope.loading = true;

      apiCall.createTemplate($scope.templateNote).success(function(result) {
        $scope.loading = false;
        $scope.success = $scope.templateNote.name;
        $scope.templateNote = {
          name: '',
          content: '',
          organization: ''
        };
        local.remove('templates');
      });

    };
  }
]);
