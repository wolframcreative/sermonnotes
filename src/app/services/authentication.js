app.factory('AuthenticationService', ['$location', '$http', '$rootScope',
	'alertService',
	function($location, $http, $rootScope, alertService) {
		return {
			login: function(credentials) {
				if (credentials.email == '' || credentials.password == '') {

					if (credentials.email == '') {
						alertService.add('error', 'Please enter a valid username');
					}

					if (credentials.password == '') {
						alertService.add('error', 'Please enter a password');
					}

				} else {
					$rootScope.alerts = [];
					$http.post('/api/login', credentials).then(function(result) {
						if (result.data.new_user) {
							$rootScope.$broadcast('new_user', true);
							local.write('logged_in', true);
						} else if (result.data.error) {
							$rootScope.alerts = [{
								'type': 'error',
								'message': result.data.error
							}];
						} else {
							local.write('logged_in', true);
							$rootScope.$broadcast('logged_in', true);
							$location.url('/dashboard');
						}
					});
				}
			},

			register: function(credentials) {
				if (credentials.email === '' || credentials.password === '') {
					$rootScope.alerts = [{
						'type': 'error',
						'message': 'Please enter a valid email and password'
					}];
				} else {
					$http.post('/api/register', credentials).then(function(result) {
						if (result.data.new_user_added) {
							window.location.href = '/auth/authorize';
						}
					});
				}
			},

			logout: function() {
				var logging_out = $http.get('/api/logout').then(function(response) {
					local.remove('logged_in');
					local.remove('notebooks');
					local.remove('templates');
					return response;
				});

				return logging_out;
			},

			isLoggedIn: function() {
				var status = $http.get('/api/logged_in').then(function(s) {
					if (s.active) {
						return s;
					} else {
						return false;
					}
				});

				return status;

			}
		};
	}
]);
