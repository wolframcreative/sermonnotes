app.factory('apiCall', ['$http', function($http) {
   return {
        
        listSermons: function () {
            return $http.get('/api/notes');
        },

        listNotebooks : function () {
            return $http.get('/api/notebooks');
        },

        createNote: function(note) {
            return $http.post('/api/note/create', note);
        },

        getTemplates: function() {
            return $http.get('/api/templates');
        },
        createTemplate: function(template) {
            return $http.post('/api/template/create', template);
        },
        deleteTemplate: function(template){
            return $http.post('/api/template/delete', template);
        },
        updateTemplate: function(template){
            return $http.post('/api/template/update',template);
        }
        
   }
}]);