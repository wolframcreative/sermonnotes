app.controller('HeaderController', ['$scope', '$rootScope', '$location',
  'AuthenticationService', 'alertService',
  function($scope, $rootScope, $location, AuthenticationService,
    alertService) {


    var local_logged_in = local.get('logged_in');

    $rootScope.$on('logged_in', function() {
      $scope.loggedIn = true;
    });

    $scope.closeAlert = function(e) {
      alertService.closeAlert(e);
    }

    if (!local_logged_in) {
      AuthenticationService.isLoggedIn().then(function(s) {
        $scope.loggedIn = s;
      });
    } else {
      $scope.loggedIn = local_logged_in;
    }

    $scope.logOut = function() {
      AuthenticationService.logout().then(function(s) {
        $scope.loggedIn = false;
        $location.path('/');
      });
    };
  }
]).directive('snHeader', function() {
  return {
    templateUrl: 'src/app/views/header.ng'
  }
});
