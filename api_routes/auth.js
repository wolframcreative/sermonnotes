var databaseUrl = (process.env.DBURL) ? process.env.DBURL :
  "sermonNotes:sermonNotes@dharma.mongohq.com:10045/sermonNotes",
  collections = ["users"],
  db = require("mongojs").connect(databaseUrl, collections),
  oa = require('evernote'),
  client = new oa.Evernote.Client({
    consumerKey: 'etanlubeck',
    consumerSecret: '3d752e8f698126ad',
    sandbox: false // Optional (default: true)
  }),
  md5 = require('md5-node'),
  devToken =
  'S=s4:U=4e672:E=14eecc2dfce:C=1479511b358:P=1cd:A=en-devtoken:V=2:H=da36fa015ce1d24700dadbdb43ff5522',
  _ = require('lodash');

exports.login = function(req, res, next) {
  var payload = {
    email: req.body.email
  };

  if (req.body.password) {
    payload.password = md5(req.body.password)
  }

  db.users.findOne({
    email: payload.email
  }, function(err, result) {
    var message;
    if (!err) {
      if (result) {
        if (result.password === payload.password) {
          req.session.current_user = result;
          message = {
            'logged_in': true
          };
        } else {
          message = {
            'error': 'invalid password. Did you forget you\'re password?'
          };
        }
      } else {
        message = {
          'new_user': true
        };
      }

      res.json(message);
    } else {
      res.json(err);
    }
  });

};

exports.register = function(req, res, next) {

  var password = md5(req.body.password),
    email = req.body.email;

  db.users.findOne({
    email: email
  }, function(errz, resz) {
    if (_.isObject(resz)) {
      res.json({
        'error': 'User exists'
      });
    } else {
      db.users.insert({
        email: email,
        password: password
      }, function(err, results) {
        if (results) {
          req.session.current_user = results[0];
          res.json({
            'new_user_added': true,
            'logged_in': true
          });
        } else {
          res.json({
            'error': 'Couldn\'t add user'
          })
        }
      });
    }
  })
};

exports.isLoggedIn = function(req, res, next) {
  if (!_.isUndefined(req.session.current_user)) {
    res.status('200').send({
      active: true
    });
  } else {
    res.status('200').send( {
      active: false
    });
  }
};


exports.logout = function(req, res) {
  if (req.session.current_user) {
    req.session.destroy();
    res.status('200').send({
      active: false
    });
  } else {
    res.status('200').send({
      active: false
    });
  }
};

exports.authorize = function(req, res) {

  if (!req.session.current_user || _.isUndefined(req.session.current_user.access_token) ||
    req.session.current_user.access_token === "") {
    var redirect_url = (!_.isUndefined(process.env.EVERNOTE_REDIRECT)) ?
      process
      .env.EVERNOTE_REDIRECT : "sermon-notes.org";
    client.getRequestToken('http://' + redirect_url + '/auth/authorized',
      function(error, oauthToken, oauthTokenSecret, results) {

        req.session.oauth = {};
        req.session.oauth.oauthTokenSecret = oauthTokenSecret;

        var url = client.getAuthorizeUrl(oauthToken);
        res.redirect(url);

      });

  } else {
    res.redirect('/dashboard');
  }



};

exports.authorized = function(req, res) {

  var oauthToken = req.query.oauth_token,
    oauthTokenSecret = req.session.oauth.oauthTokenSecret,
    oauthVerifier = req.query.oauth_verifier;


  client.getAccessToken(oauthToken, oauthTokenSecret, oauthVerifier, function(
    error, oauthAccessToken, oauthAccessTokenSecret, results) {
    if (!error) {
      var user = req.session.current_user;
      user.access_token = oauthAccessToken;
      db.users.update({
        email: user.email
      }, {
        $set: {
          access_token: oauthAccessToken
        }
      }, function(err, results) {
        res.redirect('/dashboard');
      });
    } else {
      res.send(error.data);
    }
  });
};
