var databaseUrl = (process.env.DBURL) ? process.env.DBURL :
  "sermonNotes:sermonNotes@dharma.mongohq.com:10045/sermonNotes",
  collections = ["users", "notes", "templates"],
  db = require("mongojs").connect(databaseUrl, collections);

exports.createTemplate = function(req, res) {
  if (req.session.current_user) {
    db.templates.find({
      templateOwner: req.session.current_user._id
    }, function(err, results) {
      if (err) {
        res.status('404').send(err);
      } else {

        if (results.length > 0) {
          db.templates.insert({
            templateOwner: req.session.current_user._id,
            name: req.body.name,
            content: req.body.content,
            organization: req.body.organization
          }, function(erz, rez) {
            if (err) {
              res.status('404').send(erz);
            } else {
              res.status('200').send(rez);
            }
          });
        } else {
          db.templates.insert({
            templateOwner: req.session.current_user._id,
            name: req.body.name,
            content: req.body.content,
            organization: req.body.organization
          }, function(erz, rez) {
            if (err) {
              res.status('404').send(erz);
            } else {
              res.status('200').send(rez);
            }
          });
        }
      }
    })
  }
};

exports.listTemplates = function(req, res) {
  if (req.session.current_user) {
    db.templates.find({
      templateOwner: req.session.current_user._id
    }, function(err, results) {
      if (err) {
        res.status('404').send(err);
      } else {
        if (results.length > 0) {
          res.status('200').send(results);
        } else {
          res.status('200').send({
            results: 'no results found'
          });
        }
      }
    });
  }
};



exports.updateTemplate = function(req, res, next) {
  if (req.session.current_user) {
    res.status('200').send({});
  }
};

exports.deleteTemplate = function(req, res) {
  if (req.session.current_user) {
    db.templates.remove({
      _id: req.body.id
    }, 1);
    db.templates.find({
      _id: req.body.id
    }, function(err, rez) {
      if (err) {
        res.status('404').send(err);
      } else {
        if (!rez.length) {
          res.status('200').send({
            deleted: true
          });
        }
      }
    });
  }
};
