var databaseUrl = (process.env.DBURL) ? process.env.DBURL :
	"sermonNotes:sermonNotes@dharma.mongohq.com:10045/sermonNotes",
	collections = ["users", "notes"],
	db = require("mongojs").connect(databaseUrl, collections),
	oa = require('evernote'),
	_ = require('underscore'),
	$ = require('jquerygo');


var bitlyClient = require('bitly2');
// Replace proccess.env.bitly_user & process.env.bitly_password with your own credentials!
var bitly = new bitlyClient(process.env.BITLY_USER, process.env.BITLY_PASSWORD,
	function(error) {
		if (!error) {
			console.log('logged in to bitly');
		}
	});

exports.listNotebooks = function(req, res) {
	db.users.findOne({
		email: req.session.current_user.email
	}, function(err, results) {
		if (!err) {

			var client = new oa.Evernote.Client({
				consumerKey: 'etanlubeck',
				consumerSecret: '3d752e8f698126ad',
				sandbox: false,
				token: results.access_token
			});

			var noteStore = client.getNoteStore();
			notebooks = noteStore.listNotebooks(function(errz, notebooks) {
				if (errz) {
					res.status('399').send(errz);
				} else {
					res.status('200').send(notebooks);
				}
			});
		} else {
			res.status('399').send(err);
		}
	});

};

exports.listNotes = function(req, res) {
	if (!req.session.current_user.access_token) {

		db.users.findOne({
			email: req.session.current_user.email
		}, function(err, results) {
			if (!err) {


				var client = new oa.Evernote.Client({
					consumerKey: 'etanlubeck',
					consumerSecret: '3d752e8f698126ad',
					sandbox: false,
					token: results.access_token
				});

				var noteStore = client.getNoteStore();
				noteStore.listNotebooks(function(errz, notebooks) {

					if (errz) {
						res.status('399').send(errz);
					} else {
						res.status('200').send(notebooks);
					}
				});

			} else {
				res.send(err);
			}
		});

	} else if (req.session.current_user.access_token) {
		var client = new oa.Evernote.Client({
			consumerKey: 'etanlubeck',
			consumerSecret: '3d752e8f698126ad',
			sandbox: false,
			token: req.session.current_user.access_token
		});

		var noteStore = client.getNoteStore();
		noteStore.listNotebooks(function(errz, notebooks) {

			if (errz) {
				res.status('399').send(errz);
			} else {
				res.status('200').send(notebooks);
			}
		});
	} else {
		res.status('200').send({
			logged_in: false
		});
	}
};

exports.createNote = function(req, res) {

	if (req.body) {

		var temp = req.body;
		//  var temp = {
		// 	title : "Sermon Test",
		// 	date : "January 17, 2014",
		// 	teacher : "Andres Carias",
		// 	sermon_name : "Restoring Worship",
		// 	bible_reference : "Mark 11:12-18",
		// 	note_content : "",
		//  guid : ""
		// };


		temp.bible_reference = (temp.bible_reference.match('&')) ? temp.bible_reference
			.replace('&', '&amp;') : temp.bible_reference;
		temp.sermon_name = (temp.sermon_name.match('&')) ? temp.sermon_name.replace(
			'&', '&amp;') : temp.sermon_name;
		temp.note_content = (temp.note_content.match('BLANKFILL')) ? temp.note_content
			.replace(/BLANKFILL/gi,
				'<u><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></u>'
			) :
			temp.note_content;

		if (!req.session.current_user.access_token) {

			db.users.findOne({
				email: req.session.current_user.email
			}, function(err, results) {
				if (!err) {
					res.status('200').send(results);
					console.log('somehow i didn\'t setup access trapping.');
				}
			});
		} else {
			var note = new oa.Evernote.Note();
			var templateBuilder = temp.note_template;
			templateBuilder = templateBuilder.replace(/\[\[date\]\]/, temp.date).replace(
					/\[\[teacher\]\]/, temp.teacher).replace(/\[\[sermon_name\]\]/, temp.sermon_name)
				.replace(/\[\[bible_reference\]\]/, temp.bible_reference).replace(
					/\[\[note_content\]\]/, temp.note_content);

			note.title = temp.title + ' - ' + temp.sermon_name;
			note.content =
				'<?xml version="1.0" encoding="utf-8"?><!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd"><en-note>' +
				templateBuilder + '</en-note>';

			note.notebookGuid = temp.guid;

			var client = new oa.Evernote.Client({
				consumerKey: 'etanlubeck',
				consumerSecret: '3d752e8f698126ad',
				sandbox: false,
				token: req.session.current_user.access_token
			});

			var noteStore = client.getNoteStore(),
				userStore = client.getUserStore();

			noteStore.createNote(note, function(err, createdNote) {
				if (err) {
					res.send(err);
				} else {
					userStore.getUser(function(err, user) {
						if (!err) {
							noteStore.shareNote(createdNote.guid, function(err, shareKey) {
								// https://[service]/shard/[shard ID]/sh/[noteGuid]/[shareKey]/
								bitly.get('shorten', {
									'domain': 'chrch.ly',
									'longUrl': 'https://www.evernote.com/shard/' + user.shardId +
										'/sh/' + createdNote.guid + '/' + shareKey + '/'
								}, function(error, result) {
									if (!error) {

										bitly.get('user/link_edit', {
											'link': result.data.url,
											'title': note.title,
											'edit': 'title'
										}, function(errz, rez) {
											if (!errz) {
												res.status('200').send(rez);
											}
										});
									} else {
										res.status('200').send(error);
									}
								});
							});
						}
					});

				}
			});
		}

	}
};
