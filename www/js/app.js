var app = angular.module('app', ['ngRoute','ngSanitize','ngAnimate','ui.bootstrap']);


function localConstructor() {

    this.write = function (key, obj) {
        localStorage.setItem(key, JSON.stringify(obj));
        return this;
    };
    this.get = function (key) {
        var obj = JSON.parse(localStorage.getItem(key));
        return obj;
    };
    this.clear = function () {
        localStorage.clear();
        return this;
    };
    this.remove = function (key) {
        localStorage.removeItem(key);
        return this;
    };
};

var local = new localConstructor();
app.config(['$routeProvider', '$locationProvider', function($routeProvider,
	$locationProvider) {

	$locationProvider.html5Mode(true);

	$routeProvider.when('/', {
		templateUrl: '/src/app/views/home.ng',
		controller: 'HomeController'
	});

	$routeProvider.when('/login', {
		templateUrl: '/src/app/views/login.ng',
		controller: 'LoginController'
	});

	$routeProvider.when('/create/note/:notebookID', {
		templateUrl: '/src/app/views/note.ng',
		controller: 'NoteController',
	});

	$routeProvider.when('/create/template', {
		templateUrl: '/src/app/views/templateNote.ng',
		controller: 'TemplateController'
	});

	// $routeProvider.when('/settings', {
	//   templateUrl: '/src/app/views/settings.ng',
	//   controller: 'SettingsController'
	// });

	// $routeProvider.when('/template/edit/:id', {
	//   templateUrl: 'src/app/views/template/editTemplate.ng',
	//   controller: 'EditTemplateController'
	// });

	// $routeProvider.when('/template/copy/:id', {
	//   templateUrl: 'src/app/views/template/copyTemplate.ng',
	//   controller: 'CopyTemplateController'
	// });

	// $routeProvider.when('/template/delete/:id', {
	//   templateUrl: 'src/app/views/template/editTemplate.ng',
	//   controller: 'EditTemplateController'
	// });

	$routeProvider.when('/dashboard', {
		templateUrl: '/src/app/views/list.ng',
		controller: 'ListController'
	});

	$routeProvider.otherwise({
		redirectTo: '/'
	});
}]);

app.controller("HomeController", ['$scope', '$location', '$rootScope',
  'AuthenticationService',
  function($scope, $location, $rootScope, AuthenticationService) {
    $scope.pageClass = 'page-home';
    $scope.credentials = {
      email: "",
      password: ""
    };

    $rootScope.$on('new_user', function() {
      $scope.newUser = true;
    });

    if (local.get('logged_in')) {
      $location.url('/dashboard');
    }

    $scope.login = function() {
      AuthenticationService.login($scope.credentials);
    };

    $scope.register = function() {
      AuthenticationService.register($scope.credentials);
    }

    $scope.logout = function() {
      AuthenticationService.logout();
    };

  }
]);

app.controller("ListController", ['$scope', 'apiCall', function($scope, apiCall) {
  $scope.pageClass = 'page-dashboard';
  var note_templates = local.get('templates'),
    notebook_list = local.get('notebooks');


  if (!notebook_list) {
    apiCall.listNotebooks().success(function(s) {
      $scope.notebooks = s;
      local.write('notebooks', s);
    }).error(function(e) {
      $scope.notebooks = e;
    });
  } else {
    $scope.notebooks = notebook_list;
  }


  if (!note_templates) {
    apiCall.getTemplates().then(function(templates) {
      if (Object.keys(templates.data).length > 0) {
        $scope.note_templates = templates.data;
        local.write('templates', templates.data);
      } else {
        $scope.noTemplates = true;
      }
    });
  } else {
    $scope.note_templates = note_templates;
  }

  $scope.deleteTemplate = function(event) {
    var _id = $(event.target).data('id');
    event.preventDefault();
    apiCall.deleteTemplate({
      id: _id
    }).then(function(template) {
      console.log(template);
    });
  }
}]);

app.controller("LoginController", ['$scope', '$location',
	'AuthenticationService',
	function($scope, $location, AuthenticationService) {
		$scope.credentials = {
			username: "",
			password: ""
		};
		$scope.login = function() {
			AuthenticationService.login($scope.credentials);
		}
	}
]);

app.controller("NoteController", ['$scope', '$interpolate', '$location',
  '$routeParams',
  '$http', 'apiCall',
  function($scope, $interpolate, $location, $routeParams, $http, apiCall) {
    $scope.pageClass = 'page-create';

    var converter = new Showdown.converter({
      extensions: ['youversion']
    });
    $scope.note = {
      title: "",
      date: "",
      teacher: "",
      sermon_name: "",
      bible_reference: "",
      note_content: "",
      note_template: "",
      note_converted: "",
      note_organization: ""
    };


    $scope.convert = function() {
      $scope.note.note_content = converter.makeHtml($scope.note.note_content);
    }

    $scope.preview = function() {
      var templateParse = $scope.selectedOption;

      templateParse = templateParse.replace('[[title]]', '{{note.title}}')
        .replace('[[date]]', '{{note.date}}')
        .replace('[[teacher]]', '{{note.teacher}}')
        .replace('[[sermon_name]]', '{{note.sermon_name}}')
        .replace('[[bible_reference]]',
          '{{note.bible_reference}}')
        .replace('[[note_content]]', '{{note.note_content}}');
      return templateParse;
    }

    $scope.create = function() {
      $scope.loading = true;
      $scope.note.guid = $routeParams.notebookID;
      $scope.note.note_template = $scope.selectedOption;
      // $scope.note.note_organization = 'rockypeak';

      apiCall.createNote($scope.note).success(function(body, statusCode) {
        $scope.loading = false;
        $scope.note = {
          title: "",
          date: "",
          teacher: "",
          sermon_name: "",
          bible_reference: "",
          note_content: ""
        };
        $scope.bitlyUrl = body.data.link_edit.link;
      });
    };

    if (local.get('templates').length === 0 || typeof local.get('templates') ===
      'undefined') {

      apiCall.getTemplates().then(function(templates) {
        if (Object.keys(templates.data).length > 0) {
          $scope.note_templates = templates.data;
          $scope.selectedOption = $scope.note_templates[0].content;
        } else {
          $scope.noTemplates = true;
        }


      });
    } else {
      $scope.note_templates = local.get('templates');
      $scope.selectedOption = $scope.note_templates[0].content;
    }

  }
]).directive('compile', ['$compile', function($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compile);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
    );
  };
}]);

function RootCtrl($rootScope, $location, alertService) {
  $rootScope.changeView = function(view) {
    $location.path(view);
  }

  // root binding for alertService
  $rootScope.closeAlert = alertService.closeAlert; 
}
RootCtrl.$inject = ['$scope', '$location', 'alertService'];
app.controller("TemplateController", ['$scope', '$location', '$routeParams',
  '$http', 'apiCall',
  function($scope, $location, $routeParams, $http, apiCall) {
    $scope.pageClass = 'page-template';
    $scope.templateNote = {
      name: '',
      content: '',
      organization: ''
    };
    $scope.createTemplate = function() {
      $scope.loading = true;

      apiCall.createTemplate($scope.templateNote).success(function(result) {
        $scope.loading = false;
        $scope.success = $scope.templateNote.name;
        $scope.templateNote = {
          name: '',
          content: '',
          organization: ''
        };
        local.remove('templates');
      });

    };
  }
]);

app.controller('FooterController', ['$scope', 'AuthenticationService', function(
  $scope, AuthenticationService) {

}]).directive('snFooter', function() {
  return {
    templateUrl: 'src/app/views/footer.ng'
  }
});

app.controller('HeaderController', ['$scope', '$rootScope', '$location',
  'AuthenticationService', 'alertService',
  function($scope, $rootScope, $location, AuthenticationService,
    alertService) {


    var local_logged_in = local.get('logged_in');

    $rootScope.$on('logged_in', function() {
      $scope.loggedIn = true;
    });

    $scope.closeAlert = function(e) {
      alertService.closeAlert(e);
    }

    if (!local_logged_in) {
      AuthenticationService.isLoggedIn().then(function(s) {
        $scope.loggedIn = s;
      });
    } else {
      $scope.loggedIn = local_logged_in;
    }

    $scope.logOut = function() {
      AuthenticationService.logout().then(function(s) {
        $scope.loggedIn = false;
        $location.path('/');
      });
    };
  }
]).directive('snHeader', function() {
  return {
    templateUrl: 'src/app/views/header.ng'
  }
});

'use strict';

app.factory('alertService', function($rootScope) {
    var alertService = {};

    // create an array of alerts available globally
    $rootScope.alerts = [];

    alertService.add = function(type, msg) {
      $rootScope.alerts.push({'type': type, 'msg': msg, close: alertService.closeAlert(this)});
    };

    alertService.closeAlert = function(index) {
      $rootScope.alerts.splice(index, 1);
    };

    return alertService;
  });
app.factory('apiCall', ['$http', function($http) {
   return {
        
        listSermons: function () {
            return $http.get('/api/notes');
        },

        listNotebooks : function () {
            return $http.get('/api/notebooks');
        },

        createNote: function(note) {
            return $http.post('/api/note/create', note);
        },

        getTemplates: function() {
            return $http.get('/api/templates');
        },
        createTemplate: function(template) {
            return $http.post('/api/template/create', template);
        },
        deleteTemplate: function(template){
            return $http.post('/api/template/delete', template);
        },
        updateTemplate: function(template){
            return $http.post('/api/template/update',template);
        }
        
   }
}]);
app.factory('AuthenticationService', ['$location', '$http', '$rootScope',
	'alertService',
	function($location, $http, $rootScope, alertService) {
		return {
			login: function(credentials) {
				if (credentials.email == '' || credentials.password == '') {

					if (credentials.email == '') {
						alertService.add('error', 'Please enter a valid username');
					}

					if (credentials.password == '') {
						alertService.add('error', 'Please enter a password');
					}

				} else {
					$rootScope.alerts = [];
					$http.post('/api/login', credentials).then(function(result) {
						if (result.data.new_user) {
							$rootScope.$broadcast('new_user', true);
							local.write('logged_in', true);
						} else if (result.data.error) {
							$rootScope.alerts = [{
								'type': 'error',
								'message': result.data.error
							}];
						} else {
							local.write('logged_in', true);
							$rootScope.$broadcast('logged_in', true);
							$location.url('/dashboard');
						}
					});
				}
			},

			register: function(credentials) {
				if (credentials.email === '' || credentials.password === '') {
					$rootScope.alerts = [{
						'type': 'error',
						'message': 'Please enter a valid email and password'
					}];
				} else {
					$http.post('/api/register', credentials).then(function(result) {
						if (result.data.new_user_added) {
							window.location.href = '/auth/authorize';
						}
					});
				}
			},

			logout: function() {
				var logging_out = $http.get('/api/logout').then(function(response) {
					local.remove('logged_in');
					local.remove('notebooks');
					local.remove('templates');
					return response;
				});

				return logging_out;
			},

			isLoggedIn: function() {
				var status = $http.get('/api/logged_in').then(function(s) {
					if (s.active) {
						return s;
					} else {
						return false;
					}
				});

				return status;

			}
		};
	}
]);

