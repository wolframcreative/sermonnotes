    dotenv = require('dotenv').load();

    var express = require('express'),
      keenIO = require('express-keenio'),
      url = require('url'),
      redis = require('redis'),
      logger = require('morgan'),
      bodyParser = require('body-parser'),
      cookieParser = require('cookie-parser'),
      session = require('express-session'),
      methodOverride = require('method-override'),
    	http = require('http'),
    	auth = require('./api_routes/auth'),
    	notes = require('./api_routes/notes'),
    	templates = require('./api_routes/templates');

    var RedisStore = require('connect-redis')(session);
    var databaseUrl = (process.env.DBURL) ? process.env.DBURL :
    	"sermonNotes:sermonNotes@dharma.mongohq.com:10045/sermonNotes",
    	collections = ["users", "notes"],
    	db = require("mongojs").connect(databaseUrl, collections),
    	redisUrl = (process.env.REDISCLOUD_URL) ? url.parse(process.env.REDISCLOUD_URL) :
    	"pub-redis-14861.us-east-1-3.2.ec2.garantiadata.com",
    _ = require('lodash');
    request = require('request');
    var redisClient = redis.createClient(redisUrl.port, redisUrl.hostname, {no_ready_check: true});
    redisClient.auth(redisUrl.auth.split(':')[1]);
    var app = express();


    	app.set('port', process.env.PORT || 3000);
    	app.use('/', express.static(__dirname + '/www'));
    	app.use('/src', express.static(__dirname + '/src'));
    	app.use('/vendor', express.static(__dirname + '/vendor'));

      if (process.env.NODE_ENV === 'development'){
        app.use(logger('dev'));
      }
      keenIO.configure({client:{projectId: process.env.KEENIO_PROJECT_ID, writeKey: process.env.KEENIO_WRITE_KEY}})
      app.use(keenIO.handleAll());
    	app.use(bodyParser.urlencoded({extended: false}));
      app.use(bodyParser.json());
    	app.use(cookieParser());
      app.use(session({
        store: new RedisStore({client: redisClient}),
        secret: 'sermonNotes',
        saveUninitialized: false,
        resave: false
      }));
    	app.use(methodOverride());

    	app.set('views', __dirname + '/www');
    	app.engine('html', require('ejs').renderFile);


    // API routing starts here
    app.post('/api/login', auth.login);
    app.post('/api/register', auth.register);
    app.get('/api/logged_in', auth.isLoggedIn);
    app.get('/api/logout', auth.logout);
    app.get('/auth/authorize', auth.authorize);
    app.get('/auth/authorized', auth.authorized);
    app.get('/api/notebooks', notes.listNotebooks);
    app.get('/api/notes', notes.listNotes);
    app.post('/api/note/create', notes.createNote);
    app.get('/api/templates', templates.listTemplates);
    app.post('/api/templates/update', templates.updateTemplate);
    app.post('/api/template/create', templates.createTemplate);
    app.post('/api/template/delete', templates.deleteTemplate);


    // Handle other routing.
    app.get('/*', function(req, res) {
      res.render('index.html');
    });

    app.listen(app.get('port'), function (){
      console.log("Express server listening on port " + app.get('port'));
    });
