//
//  YouVersion Extension
//  John 3:16   ->  <a href="youversion://bible?reference=JHN.3.16">John 3:16</a>
//

(function() {

	var youversion = function(converter) {

		return [{
			type: 'lang',
			regex: '([1|2|3]{0,1}[ ]{0,1}[A-Z][a-z]{0,10})\\\s([0-9]{1,3})\\\:([0-9]{1,3})',
			replace: function(match, leadingSlash, book, chapter, verse) {
				if (leadingSlash === '//') {
					return match;
				} else {
					var books = {
						"Judg": "JDG",
						"1 Samuel": "1SA",
						"2 Samuel": "2SA",
						"1 Kings": "1KI",
						"2 Kings": "2KI",
						"1 Chronicles": "1CH",
						"2 Chronicles": "2CH",
						"Song of Solomon": "SNG",
						"Ezekiel": "EZK",
						"Joel": "JOL",
						"Nahum": "NAM",
						"Mark": "MRK",
						"John": "JHN",
						"1 Corinthians": "1CO",
						"2 Corinthians": "2CO",
						"1 Thessalonians": "1TH",
						"2 Thessalonians": "2TH",
						"1 Timothy": "1TI",
						"2 Timothy": "2TI",
						"Philippians": "PHP",
						"Philemon": "PHM",
						"James": "JAS",
						"1 Peter": "1PE",
						"2 Peter": "2PE",
						"1 John": "1JN",
						"2 John": "2JN",
						"3 John": "3JN"
					};
					var bcv = new bcv_parser;
					bcv.set_options({
						"osis_compaction_strategy": "bcv"
					});
					var osis = bcv.parse(match).osis(),
						yvChapter;
					var bookSet = _.keys(books);

					bookSet.forEach(function(k, v) {
						if (osis.match(k)) {
							yvChapter = osis.replace(k, books[k]);
						}
					});
					var reference = (_.isUndefined(yvChapter)) ? osis : yvChapter;
					return '<a href="youversion://bible?reference=' + reference.toUpperCase() +
						'">' + match + '</a>';
				}
			}
		}]
	};

	// Client-side export
	if (typeof window !== 'undefined' && window.Showdown && window.Showdown.extensions) {
		window.Showdown.extensions.youversion = youversion;
	}
	// Server-side export
	if (typeof module !== 'undefined') module.exports = youversion;

}());
