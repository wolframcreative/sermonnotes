//
//  Twitter Extension
//  @username   ->  <a href="http://twitter.com/username">@username</a>
//  #hashtag    ->  <a href="http://twitter.com/search/%23hashtag">#hashtag</a>
//

(function(){

    var blankFill = function(converter) {
        return [
            { type: 'lang', regex: '\\B(\\\\)?BLANKFILL([\\S]+)\\b', replace: function(match, leadingSlash, username) {
                // Check if we matched the leading \ and return nothing changed if so
                if (leadingSlash === '\\') {
                    return match;
                } else {
                    return '<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>';
                }
            }},
        ];
    };

    // Client-side export
    if (typeof window !== 'undefined' && window.Showdown && window.Showdown.extensions) { window.Showdown.extensions.blankFill = blankFill; }
    // Server-side export
    if (typeof module !== 'undefined') module.exports = blankFill;

}());
